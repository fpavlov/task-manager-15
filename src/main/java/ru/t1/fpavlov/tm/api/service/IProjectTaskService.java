package ru.t1.fpavlov.tm.api.service;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IProjectTaskService {

    void bindTaskToProject(final String projectId, final String taskId);

    void removeProjectById(final String projectId);

    void unbindTaskToProject(final String projectId, final String taskId);

}
