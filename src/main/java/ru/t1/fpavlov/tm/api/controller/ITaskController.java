package ru.t1.fpavlov.tm.api.controller;

import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/**
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskController {

    void create();

    void clear();

    void removeById();

    void removeByIndex();

    void showById();

    void showByIndex();

    void showAll();

    void showAllByProjectId();

    void updateById();

    void updateByIndex();

    void changeStatusById();

    void changeStatusByIndex();

    void startById();

    void startByIndex();

    void completeById();

    void completeByIndex();

    void renderTasks(final List<Task> items);

}
