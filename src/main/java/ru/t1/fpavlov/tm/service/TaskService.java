package ru.t1.fpavlov.tm.service;


import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.exception.entity.TaskNotFoundException;
import ru.t1.fpavlov.tm.exception.field.DescriptionEmptyException;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.IndexIncorrectException;
import ru.t1.fpavlov.tm.exception.field.NameEmptyException;
import ru.t1.fpavlov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return this.taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return this.add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return this.add(new Task(name, description));
    }

    @Override
    public void clear() {
        this.taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(Comparator comparator) {
        if (comparator == null) return this.findAll();
        return this.taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(Sort sort) {
        if (sort == null) return this.findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return this.taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, final Sort sort) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (sort == null) return this.taskRepository.findAllByProjectId(projectId);
        return this.taskRepository.findAllByProjectId(projectId, sort.getComparator());
    }

    @Override
    public void remove(final Task task) {
        this.taskRepository.remove(task);
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.taskRepository.getSize()) throw new IndexIncorrectException();
        return this.taskRepository.findByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task item = this.findById(id);
        if (item == null) throw new TaskNotFoundException();
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.taskRepository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task item = this.findByIndex(index);
        if (item == null) throw new TaskNotFoundException();
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.taskRepository.getSize()) throw new IndexIncorrectException();
        return this.taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) return null;
        final Task item = this.findById(id);
        if (item == null) throw new TaskNotFoundException();
        item.setStatus(status);
        return item;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.taskRepository.getSize()) throw new IndexIncorrectException();
        if (status == null) return null;
        final Task item = this.findByIndex(index);
        if (item == null) throw new TaskNotFoundException();
        item.setStatus(status);
        return item;
    }

}
