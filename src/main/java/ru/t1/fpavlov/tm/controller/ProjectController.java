package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.controller.IProjectController;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.api.service.IProjectTaskService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService,
                             final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void create() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        this.projectService.create(name, description);
    }

    @Override
    public void clear() {
        System.out.println("Clear projects");
        projectService.clear();
    }

    @Override
    public void showAll() {
        System.out.println("Available sort:");
        System.out.println(Arrays.toString(Sort.displayValues()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> items = projectService.findAll(sort);
        int index = 1;
        System.out.println("Projects list:");
        for (final Project item : items) {
            if (item == null) continue;
            System.out.format("|%2d%s%n", index, item);
            index++;
        }
    }

    @Override
    public void removeById() {
        System.out.println("Enter project id");
        final String itemId = TerminalUtil.nextLine();
        this.projectTaskService.removeProjectById(itemId);
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter project index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final Project item = projectService.findByIndex(itemIndex - 1);
        projectTaskService.removeProjectById(item.getId());
    }

    @Override
    public void showById() {
        System.out.println("Enter project id");
        final String itemId = TerminalUtil.nextLine();
        final Project item = projectService.findById(itemId);
        System.out.println(item);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter project index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final Project item = projectService.findByIndex(itemIndex - 1);
        System.out.println(item);
    }

    @Override
    public void updateById() {
        System.out.println("Enter project id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        this.projectService.updateById(itemId, name, description);
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter project index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        this.projectService.updateByIndex(itemIndex - 1, name, description);
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        this.projectService.changeStatusById(itemId, status);
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        this.projectService.changeStatusByIndex(itemIndex - 1, status);
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        this.projectService.changeStatusById(itemId, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        this.projectService.changeStatusByIndex(itemIndex - 1, Status.IN_PROGRESS);
    }

    @Override
    public void completeById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        this.projectService.changeStatusById(itemId, Status.COMPLETED);
    }

    @Override
    public void completeByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        this.projectService.changeStatusByIndex(itemIndex - 1, Status.COMPLETED);
    }

}
