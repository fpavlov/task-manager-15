package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.controller.IProjectTaskController;
import ru.t1.fpavlov.tm.api.service.IProjectTaskService;
import ru.t1.fpavlov.tm.util.TerminalUtil;

/**
 * Created by fpavlov on 26.11.2021.
 */
public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("Enter the project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Entr the task id:");
        final String taskId = TerminalUtil.nextLine();
        this.projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("Ok");
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("Enter the project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Entr the task id:");
        final String taskId = TerminalUtil.nextLine();
        this.projectTaskService.unbindTaskToProject(projectId, taskId);
        System.out.println("Ok");
    }

}
