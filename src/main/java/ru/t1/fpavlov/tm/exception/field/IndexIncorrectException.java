package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect");
    }

}
