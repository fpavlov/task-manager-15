package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task id is empty");
    }

}
