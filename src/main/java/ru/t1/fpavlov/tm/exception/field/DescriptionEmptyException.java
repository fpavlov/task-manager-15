package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class DescriptionEmptyException extends AbstractFieldException {

    public DescriptionEmptyException() {
        super("Error! Description is empty");
    }

}
