package ru.t1.fpavlov.tm.exception.entity;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! The project wasn't found. Correct parameters of your request");
    }

}
