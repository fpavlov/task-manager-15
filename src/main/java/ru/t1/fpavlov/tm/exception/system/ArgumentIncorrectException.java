package ru.t1.fpavlov.tm.exception.system;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class ArgumentIncorrectException extends AbstractSystemException {

    public ArgumentIncorrectException() {
        super("Error! Incorrect argument");
    }

    public ArgumentIncorrectException(final String argument) {
        super("Error! Argument " + argument + "is incorrect.");
    }

}
